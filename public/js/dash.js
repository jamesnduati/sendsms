$(document).ready(function() {

    //get the registered users
    function registeredusers(){
        $.get( "/registeredusers", function( data ) {
            $( "#registeredusers" ).html( data );

        });
    }

    registeredusers();
    setInterval(function(){
        registeredusers() // this will run after every 3 seconds
    }, 3000);

    //get the my total sent messages
    function mysentmessages(){
        $.get( "/getmytotalmessages", function( data ) {
            $( "#mysentmessages" ).html( data );
        });
    }

    mysentmessages();
    setInterval(function(){
        mysentmessages() // this will run after every 3 seconds
    }, 3000);

    //get the account balance 
    function getaccountbalance(){
        $.get( "/getaccountbalance", function( data ) {
            $( "#getaccountbalance" ).html( data );
        });
    }

    getaccountbalance();
    setInterval(function(){
        getaccountbalance() // this will run after every 3 seconds
    }, 3000);

});


//enable a users
function enableUser(user_id)
{

    $.get( "/enableuser/"+user_id, function( data, status, message ) {
        location.reload();
    });

}
//disable a user
function disableUser(user_id)
{
    $.get( "/disableuser/"+user_id, function( data, status, message ) {
        location.reload();
    });
}
//enable an accout
function enableAccount(account_id)
{
    $.get("/enableaccount/"+account_id, function(data, status, message){
        location.reload();
    })
}
//disable an account
function disableAccount(account_id)
{
    $.get("/disableaccount/"+account_id, function(data, status, message){
        location.reload();
    });
}

//delete a recipient
function deleteRecipient(recipient_id)
{
    $.get("deleterecipient/"+recipient_id, function(data, status, message){
        location.reload();
    });
}
