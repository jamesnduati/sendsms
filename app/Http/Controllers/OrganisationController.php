<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

// models
use App\County;
use App\Organisation;
use App\OrganisationType;

class OrganisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgs = Organisation::all();
        $orgtypes = OrganisationType::all();
        $counties = County::all();
        $counties = json_decode(json_encode($counties), true);
        $orgs = json_decode(json_encode($orgs), true);
        $orgtypes = json_decode(json_encode($orgtypes), true);
        return view('organisations')->with(
                            array(  'counties'=>$counties,
                                    'orgs'=>$orgs,
                                    'orgtypes'=>$orgtypes
                                 )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $orgtypes = OrganisationType::all();
            $counties = County::all();
            $counties = json_decode(json_encode($counties), true);
            $orgtypes = json_decode(json_encode($orgtypes), true);
            return view('addorg')->with(array('counties'=>$counties,
                                        'orgtypes'=>$orgtypes)
                                 );
            
        } catch (Exception $e) {
            return $e;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $organisation = new Organisation;
            $organisation->org_type = $data['org_type'];
            $organisation->name  = $data['name'];
            $organisation->county = $data['county'];
            $organisation->save();
            return Redirect::to('orgs');
            
        } catch (Exception $e) {
            return $e;
        }
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
