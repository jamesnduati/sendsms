<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//adding the Maatwebsite\Excel\ExcelServiceProvider
use Excel;

//models
use App\Recipient;
use App\User;

class RecipientController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $id = Session::get('id');
            $users = User::all();
            $recipients = Recipient::where('added_by','=',$id)->get();
            $recipients = json_decode(json_encode($recipients), true);
            $users = json_decode(json_encode($users), true);
            return view('recipients')->with(array('recipients'=>$recipients,
                                                'users'=>$users));
            
        } catch (Exception $e) {
            return $e;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $id = Session::get('id');
            $recipients = Recipient::where('added_by','=',$id)->get();
            $recipients = json_decode(json_encode($recipients), true);
            return view('addrecipient')->with(array('recipients'=>$recipients));
            
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if (!empty($data)) {
                $recipient = new Recipient;
                $recipient->name = $data['name'];
                $recipient->phone = $data['phone'];
                $recipient->added_by = Session::get('id');
                $recipient->save();
                return Redirect::to('recipients');
            }
            
        } catch (Exception $e) {
            return $e;
        }
    }


    /*
    *
    *Adding an Excel file
    *
    */
    public function createExcel()
    {
        return view('addrecipientexcel');
    }

    // 
    public function postExcel(Request $request)
    {
        try {
            if($request->hasFile('recipients')){
            $path = $request->file('recipients')->getRealPath();
            Excel::load($path, function($reader) {
            // Getting all results as an array
            $results = $reader->get()->toArray();

                foreach($results as $result)
                {
                    $recipient = new Recipient;
                    $recipient->name = $result['name'];
                    $recipient->phone = "+245".$result['phone'];
                    $recipient->added_by = Session::get('id');
                    $recipient->save();
         
                }

            });
            return Redirect::to('recipients');

        }
            
        } catch (Exception $e) {

            return $e;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
             $recipient = Recipient::find($id);
            $recipient->delete();
            return Response::json(array('status' => 'success', 'data'=>"Recipient Deleted"));
        } catch (Exception $e) {
            return Response::json(array('status' => 'failure', 'data'=>$e));
        }
    }
}
