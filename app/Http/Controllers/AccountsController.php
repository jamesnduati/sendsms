<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator; 

//models
use App\Accounts;
use App\AccountType;
use App\County;
use App\Organisation;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Accounts::all();
        $accounttypes = AccountType::all();
        $orgs = Organisation::all();
        $accounts = json_decode(json_encode($accounts), true);
        $accounttypes = json_decode(json_encode($accounttypes), true);
        $orgs = json_decode(json_encode($orgs), true);
        return view('accounts')->with(array('accounts' => $accounts,
                                            'accounttypes' => $accounttypes,
                                            'orgs' => $orgs));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounttypes = AccountType::all();
        $orgs = Organisation::all();
        $accounttypes = json_decode(json_encode($accounttypes), true);
        $orgs = json_decode(json_encode($orgs), true);
        return view('addaccount')->with(array('orgs' => $orgs,
                                              'accounttypes' =>$accounttypes));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // Validate form data
        $validator = Validator::make($request->all(), [
            'organisation' => 'required',
            'account_type' => 'required',
            'status' => 'required',
            'sender_id' =>'required',
            'username'     => 'required',
            'password'  => 'required|min:6'
        ]);
        $data = $request->all();
        if (empty($data)) {
            // Authentication failed, redirect back to the login form with errors
            Session::flash('alert-danger', 'Cannot save empty fields');
            return Redirect::to('addaccount')->withErrors($validator)
                                        ->withInput($request->except('password'));
        }else{
            $account = new Accounts;
            $account->organisation = $data['organisation'];
            $account->account_type  = $data['account_type'];
            $account->sender_id = $data['sender_id'];
            $account->status = $data['status'];
            $account->username  = $data['username'];
            $account->password = $data['password'];
            $account->save();
            return Redirect::to('accounts');
        }
    }

    /**
    *Enable a user
    **/
    public function enableAccount($id)
    {
        $user = Accounts::find($id);
        $user->status = 1;
        $user->save();
        return Response::json(array('status' =>"success" ,"message"=>"User has been Enabled" ));
    }
    /**
    *Disable a user
    **/
    public function disableAccount($id)
    {
        $user = Accounts::find($id);
        $user->status = 0;
        $user->save();
        return Response::json(array('status' =>"success" ,"message"=>"User has been Disabled" ));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
