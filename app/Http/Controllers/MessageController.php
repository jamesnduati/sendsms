<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

// infobip api
use infobip\api\AbstractApiClient;
use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;
use infobip\api\client\GetSentSmsDeliveryReports;
use infobip\api\model\sms\mt\reports\GetSentSmsDeliveryReportsExecuteContext;

//models
use App\Accounts;
use App\Message;
use App\Recipient;
use App\User;
use App\Organisation;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
           $id = Session::get('id');
            $users = User::all();
            $messages = Message::where('sent_by','=',$id)->get();
            $messages = json_decode(json_encode($messages), true);
            $users = json_decode(json_encode($users), true);
            $orgs = Organisation::all();
            $orgs = json_decode(json_encode($orgs), true);
            //update the table
            if(!empty($messages))
                $update_table = $this->getStatus();
            return view('messages')->with(array('messages'=>$messages,
                                            'users'=>$users,
                                            'orgs'=>$orgs)); 
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $id = Session::get('id');
            $recipients = Recipient::where('added_by','=',$id)->get();
            $recipients = json_decode(json_encode($recipients), true);
            return view('sendmessage')->with(array('recipients'=>$recipients)); 
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user = User::find(Session::get('id'));
        $account = Accounts::where('organisation','=',$user->organisation)->first();
        $recipients = Recipient::find($data['recipient']);
        $recipients = json_decode(json_encode($recipients), true);
        $myrecipients=[];
        foreach ($recipients as $recipient) {
            $myrecipient[] = $recipient['phone'];
            $myrecipients = $myrecipient;
        }

        $username = $account->username;
        $password = $account->password;
        $to = $myrecipients;
        $from = $account->sender_id;

        // Initializing SendSingleTextualSms client with appropriate configuration
        $client = new SendSingleTextualSms(new BasicAuthConfiguration($username, $password));

        // Creating request body
        $requestBody = new SMSTextualRequest();
        $requestBody->setFrom($from);
        $requestBody->setTo($to);
        $requestBody->setText($data['message']);

        // Executing request
        try {
            $response = $client->execute($requestBody);
            $sentMessageInfo = $response->getMessages()[0];
  
            $message  = new Message;
            $message->message_id = $sentMessageInfo->getMessageId();
            $message->recipient = '+'.$sentMessageInfo->getTo();
            $message->sender_id = $from;
            $message->message = $data['message'];
            $message->sent_by = Session::get('id');
            $message->organisation = $user->organisation;
            $message->status = $sentMessageInfo->getStatus()->getName();
            $message->sent_at = date('Y-m-d H:i:s');
            $message->price = "0";
            $message->currency = "0";
            $message->save();
            //return to messages page
            return Redirect::to('messages');
        } catch (Exception $exception) {
            $result = array(
                "HTTP status code: " . $exception->getCode(),
                "Error message: " . $exception->getMessage()
                );
            return Redirect::to('sendmessage');
        }

    }
    //updating the messages table
    public function getStatus()
    {
        try {
            $user = User::find(Session::get('id'));
            $account = Accounts::where('organisation','=',$user->organisation)->first();
            $username = $account->username;
            $password = $account->password;
            // Initializing GetSentSmsDeliveryReports client with appropriate configuration
            $client = new GetSentSmsDeliveryReports(new BasicAuthConfiguration($username, $password));
            // Creating execution context
            $context = new GetSentSmsDeliveryReportsExecuteContext();
            // Executing request
            $response = $client->execute($context);
            //looping through the message statuses
            for ($i = 0; $i < count($response->getResults()); ++$i) {
                $result = $response->getResults()[$i];
                $message = Message::where('message_id','=',$result->getMessageId())->first();
                $message->status = $result->getStatus()->getName();
                $message->sent_at = $result->getSentAt()->format('Y-m-d H:i:s') ;
                $message->price = $result->getPrice()->getPricePerMessage();
                $message->currency = $result->getPrice()->getCurrency();
                $message->save();
            } 
        } catch (Exception $e) {
            return $e;
            
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
