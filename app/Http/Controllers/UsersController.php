<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

//extensions
use File;
use Mail;

// models
use App\User;
use App\UserType;
use App\County;
use App\Organisation;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = User::all();
        $usertypes = UserType::all();
        $users = json_decode(json_encode($users), true);
        $usertypes = json_decode(json_encode($usertypes), true);
        return view('users')->with(array('users'=>$users, 
                                         'usertypes'=>$usertypes
                                         )
                                    );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Respons 
     */
    public function create()
    {
        $usertypes = UserType::all();
        $organisations = Organisation::all();
        $usertypes = json_decode(json_encode($usertypes), true);
        $organisations = json_decode(json_encode($organisations), true);
        return view('adduser')->with(array('usertypes'=>$usertypes,
                                            'organisations'=>$organisations
                                            )
                                    );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //getting all the organisations
        $orgs = Organisation::all();
        $orgs = json_decode(json_encode($orgs), true);
        //gettting the data from the form
        $data = $request->all();
        $user = new User;
        $user->surname = $data['surname'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->user_type = $data['usertype'];
        $user->password = Hash::make($data['password']);
        $user->change_password = 1;
        $user->id_number = $data['id_number'];
        $user->gender = $data['gender'];
        $user->enabled = $data['enabled'];
        $user->organisation = $data['organisation'];
        $user->save();
        //getting the user's organisation
        $organisation = Organisation::where('id','=',$data['organisation'])->first();
        // echo $organisation->name;
        // die();

        //send an email to the user after registration
        Mail::send('emails.welcome', ['user' => $user, 'orgs'=>$orgs, 'data'=>$data], function ($message) use ($user) {
            $message->from('hello@sendsms.co.ke', 'Welcome to SendSms.');

            $message->to($user->email, $user->first_name.' '.$user->last_name)
                    ->subject('User account registration');
        });
        return Redirect::to('users');
    }
    /**
    *Enable a user
    **/
    public function enableUser($id)
    {
        $user = User::find($id);
        $user->enabled = 1;
        $user->save();
        return Response::json(array('status' =>"success" ,"message"=>"User has been Enabled" ));
    }
    /**
    *Disable a user
    **/
    public function disableUser($id)
    {
        $user = User::find($id);
        $user->enabled = 0;
        $user->save();
        return Response::json(array('status' =>"success" ,"message"=>"User has been Disabled" ));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
