<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//infobip classes
use infobip\api\client\GetAccountBalance;
use infobip\api\configuration\BasicAuthConfiguration;

// models to be users
use App\Accounts;
use App\User;
use App\UserType;
use App\Message;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }


    // Dash api for users registered
    public function getUsers()
    {
        $data = User::whereIn('user_type', array(2, 3))->count();
        return $data;

    }

    // Dash api for total sms sent
    public function getTotalMessages()
    {
        $data = Message::whereIn('user_type', array(2, 3))->count();
        return $data;

    }

    // Dash api for total sms sent
    public function getMyTotalMessages()
    {
        $id = Session::get('id');
        $data = Message::where('sent_by','=', $id)->count();
        return $data;

    }

    // Dash api for account balance
    public function getAccountBalance()
    {
        $user = User::find(Session::get('id'));
        $account = Accounts::where('organisation','=',$user->organisation)->first();
        $username = $account->username;
        $password = $account->password;
        // Initializing GetAccountBalance client with appropriate configuration
        $client = new GetAccountBalance(new BasicAuthConfiguration($username, $password));
        // Executing request
        $response = $client->execute();

        // echo 'accountBalance = ' . $response->getBalance() . ' ' . $response->getCurrency();
        $data = $response->getBalance();
        return $data;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
