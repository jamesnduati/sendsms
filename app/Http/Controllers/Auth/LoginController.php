<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\UserType;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo  = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * post the login credentials.
     *
     * @return view
     */
    public function login()
    {
        return view('login');
    }
    /**
     * post the login credentials.
     *
     * @return Request
     */
    public function postLogin(Request $request)
    {
        // Validate form data
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6'
        ]);
        // echo Hash::make($request['password']);
        // die();
        $data = $request->all();
        // Attempt to authenticate user
        // If successful, redirect to their intended location
       if(Auth::attempt(array('email'=>$data['email'],'password'=>$data['password']))){
            //get the users first name and last name
            $user = User::where('email','=',$data['email'])->first();
            //storing data in the session
            Session::put('id', $user->id);
            Session::put('email', $user->email);
            Session::put('first_name', $user->first_name);
            Session::put('last_name', $user->last_name);
            $user_type = UserType::find($user->user_type);
            Session::put('user_type', $user_type->name);
            // Session::put('profile_photo', $user->profile_photo);
            if($user->change_password == 0){
                Session::flash('alert-success', 'Welcome '.$user->first_name.' '.$user->last_name);
                  return Redirect::to('dashboard');
            }else{
                Session::flash('alert-danger', 'Please change your password before proceeding');
                  return Redirect::to('changepassword');
            }
        }else{
            // Authentication failed, redirect back to the login form with errors
            return Redirect::to('login')->withErrors($validator)
                                        ->withInput($request->except('password'));
        }
    }


    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */

    public function logout()
    {
      $user = Auth::user();
      Log::info('User Logged Out. ', [$user]);
      Auth::logout();
      Session::flush();
      return Redirect::to('/');
    }
}


