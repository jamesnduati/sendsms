<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


//models
use App\User;
use App\UserType;

class ApiController extends Controller
{
    

    public function Login(Request $request)
    {
        // Validate form data
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6'
        ]);
        $data = $request->all();
        if(!empty($data)){
        // Attempt to authenticate user
        // If successful, redirect to their intended location
           if(Auth::attempt(array('email'=>$data['email'],'password'=>$data['password']))){
                //get the users first name and last name
                $user = User::where('email','=',$data['email'])->first();
                $user->token = $mCode = $this->getRandomString(20);
                $user->save();
                $user_type = UserType::find($user->user_type);
                //storing data in the array
                $user_data = [
                    'email' => $user->email,
                    'first_name'=> $user->first_name,
                    'last_name' => $user->last_name,
                    'user_type' => $user_type->name,
                    'token' => $user->token,
                    'changepassword'=> $user->change_password
                    ];
                return Response::json(array('status' => 'success' , 'message'=> $user_data ));
            }else{
                // Authentication failed, redirect back to the login form with errors
                return Response::json(array('status' => 'failure' , 'message'=>'Wrong credentials'));
            }
        }
        // Authentication failed, redirect back to the login form with errors
        return Response::json(array('status' => 'failure' , 'message'=>'No credentials entered'));

    }

    //generating a random string
    public function getRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    //

    // logout the user
    public function Logout($token)
    {

        $data = $token;
        if(!empty($data)){
            //get the users first name and last name
            $user = User::where('token','=',$data['token'])->first();
            $user->token = $this->getRandomString(20);
            $user->save();
        // logout procedure a success
            return Response::json(array('status' => 'success' , 'message'=> 'User logged out' ));
        }
        // logout procedure failed
        return Response::json(array('status' => 'failure' , 'message'=>'Error!'));

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
