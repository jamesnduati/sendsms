<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Requests;

use File;
use Mail;

// models
use App\User;
use App\Organisation;

class ForgotPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('changepassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('forgot');
    }
    /**
     * store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        //getting all the organisations
        $orgs = Organisation::all();
        $orgs = json_decode(json_encode($orgs), true);
        //getting all the request data
        $data = $request->all();
        if (!empty($data)) {
            $user = User::where('email','=', $data['email'])->first();
            $newPassword = $this->getRandomString(8);
            $user->password = Hash::make($newPassword);
            $user->change_password = 1;
            $user->save();

            //send an email to the user after registration
            Mail::send('emails.forgot', ['user' => $user, 'new_password'=>$newPassword, 'orgs'=>$orgs], function ($message) use ($user) {
                $message->from('hello@semasms.co.ke', 'User Password for SendSms changed.');

                $message->to($user->email, $user->first_name.' '.$user->last_name)
                        ->subject('New user password');
            });
            return Redirect::to('login');
        }else{
            return view('forgot');
        }

    }


    //generating a random string
    public function getRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate form data
        $validator = Validator::make($request->all(), [
            'old_password'  => 'required',
            'new_password'  => 'required|min:6',
            'confirm_password'  => 'required|same:new_password'
        ]);
        $data = $request->all();
        $user = User::find(Session::get('id'));
        //checking whether the password matches
        if(Hash::check($data['old_password'], $user->password)){
            //updating the user password
            $user->password = Hash::make($data['new_password']);
            $user->change_password = 0;
            $user->save();
            // giving the user a success message alert
            Session::flash('alert-success', 'Password has been changed successfully');
              return Redirect::to('dashboard');
        }else{
            //echo "Not Okay";
            die();
            // Authentication failed, redirect back to the login form with errors
            return Redirect::to('changepassword')->withErrors($validator)
                                        ->withInput($request->except('password'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
