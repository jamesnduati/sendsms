@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Add User</h3>
        			<form action="{{url('adduser')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-4">
                				<label>Surname:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
				                  <input type="text" class="form-control" name="surname" placeholder="Surname">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>First Name:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
				                  <input type="text" class="form-control" name="first_name" placeholder="First Name">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Last Name:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
				                  <input type="text" class="form-control" name="last_name" placeholder="Last Name">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-4">
                				<label>Email:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-envelope"></i>
				                  </div>
				                  <input type="text" class="form-control" name="email" placeholder="Email">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Phone Number:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-phone-square"></i>
				                  </div>
				                  <input type="text" class="form-control" name="phone_number" placeholder="Phone Number">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>User Type:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-tag"></i>
				                  </div>
		                          	<select name="usertype" class="form-control" placeholder="usertype">
									@foreach($usertypes as $usertype)
								      <option value="{{ $usertype['id'] }}">{{ $usertype['name'] }}</option>
									@endforeach
									</select>
				                  <!-- <input type="text" class="form-control" name="user_type" placeholder="User Type"> -->

				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-3">
                				<label>Password:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-phone-square"></i>
				                  </div>
				                  <input type="Password" class="form-control" name="password" placeholder="Password">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				   	<div class="form-group col-sm-3">
                				<label>Id Number:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-phone-square"></i>
				                  </div>
				                  <input type="text" class="form-control" name="id_number" placeholder="Id Number">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-3">
                				<label>Gender:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-tag"></i>
				                  </div>
		                          	<select name="gender" class="form-control" placeholder="gender">
								      <option value="Male">Male</option>
								      <option value="Female">Female</option>
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				 	<div class="form-group col-sm-3">
                				<label>Status:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-tag"></i>
				                  </div>
		                          	<select name="enabled" class="form-control" placeholder="enabled">
								      <option value="0">Disabled</option>
								      <option value="1">Enabled</option>
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->

        				<div class="row">
    					 	<div class="form-group col-sm-6">
                				<label>Organisation:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-university"></i>
				                  </div>
		                          	<select name="organisation" class="form-control" placeholder="Organisation">
									@foreach($organisations as $organisation)
								      <option value="{{ $organisation['id'] }}">{{ $organisation['name'] }}</option>
									@endforeach
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->

        				</div><!-- /.input group -->

        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('users') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add User</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection