 @extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">All Organisations</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="organisations" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Organisation Type</th>
							<th>Name</th>
							<th>County</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($orgs as $org)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                @foreach($orgtypes as $orgtype)
			                	@if($org['org_type'] == $orgtype['id'])
			                		<td>{{ $orgtype['name'] }}</td>
		                		@endif
			                @endforeach	
			                <td>{{ $org['name']}}</td>
			                @foreach($counties as $county)
			                	@if($org['county'] == $county['id'])
			                		<td>{{ $county['county_name'] }}</td>
		                		@endif
			                @endforeach	              
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection