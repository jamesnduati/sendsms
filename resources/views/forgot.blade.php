<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SemaSms | Forgot Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/dist/css/admin-lte.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/plugins/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<div class="header" style="height: 20px;">
  <div class="col-xs-6">

  </div>
</div>
<body class="hold-transition login-page" style="font-family: 'ContextLightSSi';">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/')}}"><img src="images/semasms.png" width="200" height="100"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Forgot Password for <b>SemaSms</b></p>
      @if ($alert = Session::get('alert-success'))
            <div class="alert alert-success">
                <span class="glyphicon glyphicon-ok"></span>
                <em>{!! $alert !!}</em>
            </div>
      @endif

    <form action="{{ url('forgot')}}" method="post">
      <div class="form-group has-feedback">
         {{ csrf_field() }}
      </div>
      <div class="form-group has-feedback">
        <input type="email"  name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($error = $errors->first('email'))
          <div class="alert alert-danger">{{ $errors->first('email') }}</div>
        @endif
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-success btn-block btn-flat">Send</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.forgot-password-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src=" {{ asset('/bower_components/admin-lte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/bower_components/admin-lte/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/bower_components/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>

</body>
</html>
