 @extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">All Organisation Types</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="usertable" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Organisation Type Name</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($orgtypes as $orgtype)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                <td>{{ $orgtype['name']}}</td>           
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection