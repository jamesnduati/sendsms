@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Change Password</h3>
        			      @if ($alert = Session::get('alert-success'))
				            <div class="alert alert-success">
				                <span class="glyphicon glyphicon-ok"></span>
				                <em>{!! $alert !!}</em>
				            </div>
					      @endif
        			<form action="{{url('changepassword')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>Old Password:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-lock"></i>
				                  </div>
  				                  <input type="password" class="form-control" name="old_password" placeholder="Old Password">
  				                   @if ($error = $errors->first('old_password'))
							          <div class="alert alert-danger">{{ $errors->first('old_password') }}</div>
							       @endif
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				</div><!-- /.input group -->
          				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>New Password:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-lock"></i>
				                  </div>
				                  <input type="password" class="form-control" name="new_password" placeholder="New Password">
				                   @if ($error = $errors->first('new_password'))
							          <div class="alert alert-danger">{{ $errors->first('new_password') }}</div>
							       @endif
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				</div><!-- /.input group -->
          				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>Confirm Password</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-lock"></i>
				                  </div>
				                  <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
				                   @if ($error = $errors->first('confirm_password'))
							          <div class="alert alert-danger">{{ $errors->first('confirm_password') }}</div>
							       @endif
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('dashboard') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Change</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection