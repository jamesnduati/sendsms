@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Add Recipient</h3>
        			<form action="{{url('addrecipient')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-4">
                				<label>Name:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
									<input type="text" name="name" class="form-control" placeholder="Name">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Phone:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-phone"></i>
				                  </div>
									<input type="text" class="form-control" name="phone" placeholder="+254*****">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('recipients') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add Recipient</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection