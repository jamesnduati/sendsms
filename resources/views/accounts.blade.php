 @extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">All Accounts</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="accounts" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Organisation</th>
							<th>County</th>
							<th>Status</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($accounts as $account)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                @foreach($orgs as $org)
			                	@if($account['organisation'] == $org['id'])
			                		<td>{{ $org['name'] }}</td>
		                		@endif
			                @endforeach	
			                @foreach($accounttypes as $accounttype)
			                	@if($account['account_type'] == $accounttype['id'])
			                		<td>{{ $accounttype['name'] }}</td>
		                		@endif
			                @endforeach
			                @if($account['status'] == 1)	
			                	<td>
			                		<button type="button" class="btn btn-danger" onClick="disableAccount({{ $account['id'] }})">
			                		Disable</button>
								</td>
			                @else
			                	<td>
			                		<button type="button" class="btn btn-success" onClick="enableAccount({{ $account['id'] }})">Enable</button>
			                	</td> 
			                @endif           
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection