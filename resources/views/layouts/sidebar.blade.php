<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">
                <a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i><b>DASHBOARD</b></a>
            </li>
            <!-- Optionally, you can add icons to the links -->
            <!-- <li class="active"><a href="#"><span>Link</span></a></li> -->
            <!-- <li><a href="#"><span>Another Link</span></a></li> -->
            <!-- users -->
            @if ( Session::get('user_type') == 'Administrator')
            <!-- users -->
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Users</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('users')}}"><i class="fa fa-users"></i>View Users</a>
                    </li>
                    <li>
                        <a href="{{url('adduser')}}"><i class="fa fa-user"></i>Add User</a>
                    </li>
                </ul>
            </li>
            <!-- /users -->
            <!-- organisations -->
            <li class="treeview">
                <a href="#"><i class="fa fa-university"></i><span>Organisation</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('orgs')}}"><i class="fa fa-list-alt"></i>View Organisations</a>
                    </li>
                    <li>
                        <a href="{{url('orgtypes')}}"><i class="fa fa-tags"></i>View Organisation Type</a>
                    </li>
                    <li>
                        <a href="{{url('addorg')}}"><i class="fa fa-university"></i>Add Organisation</a>
                    </li>
                    <li>
                        <a href="{{url('addorgtype')}}"><i class="fa fa-tag"></i>Add Organisation Type</a>
                    </li>
                </ul>
            </li>
            <!-- /organisations -->
            <!-- accounts -->
            <li class="treeview">
                <a href="#"><i class="fa fa-address-book"></i><span>Accounts</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('accounts')}}"><i class="fa fa-list"></i>View Accounts</a>
                    </li>
                    <li>
                        <a href="{{url('accounttypes')}}"><i class="fa fa-tags"></i>View Account Type</a>
                    </li>
                    <li>
                        <a href="{{url('addaccount')}}"><i class="fa fa-suitcase"></i>Add Account</a>
                    </li>
                </ul>
            </li>
            <!-- /accounts -->
            <!-- sms -->
            <li class="treeview">
                <a href="#"><i class="fa fa-envelope"></i><span>My Sms</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('messages')}}"><i class="fa fa-reply-all"></i>My Messages</a>
                    </li>
                    <li>
                        <a href="{{url('sendmessage')}}"><i class="fa fa-share"></i>Send Sms</a>
                    </li>
                </ul>
            </li>
            <!-- /sms -->
            <!-- Payments -->
            <li class="treeview">
                <a href="#"><i class="fa fa-money"></i><span>Payments</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('payments')}}"><i class="fa fa-shopping-cart"></i>View Payments</a>
                    </li>
                    <li>
                        <a href="{{url('renew')}}"><i class="fa fa-link"></i>Renew Subscription</a>
                    </li>
                </ul>
            </li>
            <!-- /Payments -->
            <!-- change password -->
            <li><a href="{{url('changepassword')}}"><i class="fa fa-key"></i>Change Password</a></li>
            <!-- /change password -->
            <!-- logout -->
            <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i>Logout</a></li>
            <!-- /logout -->
            @endif
            @if ( Session::get('user_type') == 'Admin')
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Users</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('myusers')}}"><i class="fa fa-users"></i>View Users</a>
                    </li>
                    <li>
                        <a href="{{url('adduser')}}"><i class="fa fa-user"></i>Add User</a>
                    </li>
                </ul>
            </li>
            <!-- /users -->
            <!-- accounts -->
            <!-- recipients -->
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Recipients</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('recipients')}}"><i class="fa fa-users"></i>View Recipients</a>
                    </li>
                    <li>
                        <a href="{{url('addrecipient')}}"><i class="fa fa-user-plus"></i>add Recipient</a>
                    </li>
                    <li>
                         <a href="{{url('addrecipientfile')}}"><i class="fa fa-users"></i>Add Excel File</a>
                    </li>
                </ul>
            </li>
            <!-- /recipients -->
            <!-- sms -->
            <li class="treeview">
                <a href="#"><i class="fa fa-envelope"></i><span>My Sms</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('messages')}}"><i class="fa fa-reply-all"></i>My Messages</a>
                    </li>
                    <li>
                        <a href="{{url('sendmessage')}}"><i class="fa fa-share"></i>Send Sms</a>
                    </li>
                </ul>
            </li>
            <!-- /sms -->
            <!-- Payments -->
            <li class="treeview">
                <a href="#"><i class="fa fa-money"></i><span>Payments</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('buysms')}}"><i class="fa fa-shopping-cart"></i>Buy Sms</a>
                    </li>
                    <li>
                        <a href="{{url('renew')}}"><i class="fa fa-link"></i>Renew Subscription</a>
                    </li>
                </ul>
            </li>
            <!-- /Payments -->
            <!-- change password -->
            <li><a href="{{url('changepassword')}}"><i class="fa fa-key"></i>Change Password</a></li>
            <!-- /change password -->
            <!-- logout -->
            <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i>Logout</a></li>
            <!-- /logout -->
            @endif
            @if ( Session::get('user_type') == 'User')
            <!-- recipients -->
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Recipients</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('recipients')}}"><i class="fa fa-users"></i>View Recipients</a>
                    </li>
                    <li>
                        <a href="{{url('addrecipient')}}"><i class="fa fa-user-plus"></i>Add Recipient</a>
                    </li>
                    <li>
                        <a href="{{url('addrecipientfile')}}"><i class="fa fa-users"></i>Add Excel File</a>
                    </li>
                </ul>
            </li>
            <!-- /recipients -->
            <!-- sms -->
            <li class="treeview">
                <a href="#"><i class="fa fa-envelope"></i><span>My Sms</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('messages')}}"><i class="fa fa-reply-all"></i>My Messages</a>
                    </li>
                    <li>
                        <a href="{{url('sendmessage')}}"><i class="fa fa-share"></i>Send Sms</a>
                    </li>
                </ul>
            </li>
            <!-- /sms -->
            <!-- Payments -->
            <li class="treeview">
                <a href="#"><i class="fa fa-money"></i><span>Payments</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('buysms')}}"><i class="fa fa-shopping-cart"></i>Buy Sms</a>
                    </li>
                    <li>
                        <a href="{{url('renew')}}"><i class="fa fa-link"></i>Renew Subscription</a>
                    </li>
                </ul>
            </li>
            <!-- /Payments -->
            <!-- change password -->
            <li><a href="{{url('changepassword')}}"><i class="fa fa-key"></i>Change Password</a></li>
            <!-- /change password -->
            <!-- Profile -->
            <!-- <li><a href="{{url('profile')}}"><i class="fa fa-user"></i>My Profile</a></li> -->
            <!-- /Profile -->
            <!-- logout -->
            <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i>Logout</a></li>
            <!-- /logout -->
            @endif
            </ul>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>