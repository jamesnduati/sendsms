<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Bulk SMS made Easy
    </div>
    <!-- Default to the left -->
    <strong>Copyright © <?php echo date('Y');?> <a href="{{ url('/')}}"> SemaSms</a>.</strong> All rights reserved.
</footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- Searchable options list -->
<script src="{{ asset ("/js/sol.js") }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#recipients').searchableOptionList({
            showSelectAll: true,
            useBracketParameters: true
        });
    });

</script>
<!-- Datatables -->
<script src="{{ asset ('/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset ('/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- responsive datatables -->
<script src="{{ asset ('/bower_components/admin-lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //users table
        $('#usertable').DataTable({
           "responsive": true,
           // "scrollY": 200,
           // "scrollX": true 
        });
        // organisations
        $('#organisations').DataTable({
           "responsive": true
           // "scrollY": 200,
           // "scrollX": true 
        });
        //messages table
        $('#messages').DataTable({
            "responsive": true
            // "scrollY": 200,
            // "scrollX": true
        });
        //recipients table
        $('#recipient').DataTable({
            "responsive": true
            // "scrollY": 200,
            // "scrollX": true
        });
        //accounts
        $('#accounts').DataTable({
            "responsive": true
            // "scrollY": 200,
            // "scrollX": true
        });
        //account types
        $('#accounttype').DataTable({
            "responsive": true
            // "scrollY": 200,
            // "scrollX": true
        });

    });
    
</script>
<!-- /Datatables -->
<!-- AdminLTE App -->
<script src="{{ asset ('/bower_components/admin-lte/dist/js/app.min.js') }}" type="text/javascript"></script>
<!-- dash js ajax calls -->
<script src="{{ asset ('/js/dash.js') }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>