<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>SemaSms | Dashboard</title>
    <!-- preventing the back button -->
    <meta http-equiv="cache-control" content="private, max-age=0, no-cache, no-store">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="pragma" content="no-store">
    <meta http-equiv="expires" content="0">
    <!-- search engine optimization -->
    <meta name="description" content="SemaSms is a sms messaging solution by James Risk Management Ltd(JRM). It offers mobile messaging services, specializing in bulkSMS services to schools, churches, NGOs, SACCOs, SMEs, corporates and other organisations that may wish to use bulk sms as a mean of mass market communication.">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('/bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{ asset('/ionicons-2.0.1/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/bower_components/admin-lte/dist/css/admin-lte.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <!-- Searchable options list -->
    <link href="{{ asset ('/css/sol.css') }}" rel="stylesheet" type="text/css" >
    <!-- Datatables -->
    <link href="{{ asset ('/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset ('/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" >
    <!-- responsive Datatables -->
    <link href="{{ asset ('/bower_components/admin-lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css" >
    <!-- skin -->
<!--     <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-black.min.css") }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('/bower_components/admin-lte/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-black">
<div class="wrapper">

    <!-- Header -->
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo"><img src="images/semasms.png" width="100" height="50"></a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->


                <!-- Notifications Menu -->

                <!-- Tasks Menu -->

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ Avatar::create(Session::get('first_name').' '.Session::get('last_name'))->toBase64() }}" class="user-image" alt="User Image" />
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Session::get('first_name').' '.Session::get('last_name')}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{ Avatar::create(Session::get('first_name').' '.Session::get('last_name'))->toBase64() }}" class="user-image" alt="User Image" />
                                <p>{{ Session::get('first_name').' '.Session::get('last_name')}}</p>
                                <p><small>{{ Session::get('user_type') }}</small></p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>