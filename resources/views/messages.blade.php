 @extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">My Sent Messages</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="messages" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Message Id</th>
							<th>Recipient</th>
							<th>Message</th>
							<th>Sent At</th>
							<th>Status</th>
							<th>Price</th>
							<th>Currency</th>
							<th>Sent By</th>
							<th>Organisation</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($messages as $message)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                <td>{{ $message['message_id'] }}</td>
							<td>{{ $message['recipient'] }}</td>
							<td>{{ $message['message'] }}</td>
							<td>{{ $message['sent_at'] }}</td>
							<td>{{ $message['status'] }}</td>
							<td>{{ $message['price'] }}</td>
							<td>{{ $message['currency'] }}</td>
			                @foreach($users as $user)
			                	@if($message['sent_by'] == $user['id'])
			                		<td>{{ $user['first_name'].' '.$user['last_name'] }}</td>
		                		@endif
			                @endforeach
			                @foreach($orgs as $org)
			                	@if($message['organisation'] == $org['id'])
			                		<td>{{ $org['name'] }}</td>
		                		@endif
			                @endforeach	            
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection