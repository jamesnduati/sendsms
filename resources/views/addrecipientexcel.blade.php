@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Add Recipient in Excel Sheet</h3>
	              	<div class="col-sm-12">
			                <div class="col-sm-12">
			                	<p>Please ensure that your excel file is in this format</p>
			                	<p>|No.|Name|Phone</p>
			                	<p>Also ensure that the numbers start with 07</p>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			<form action="{{url('addrecipientfile')}}" method="POST" enctype="multipart/form-data" files="true">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-4">
                				<label>Recipients Excel File:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
									<input type="file" name="recipients" class="form-control" placeholder="Excel File">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('recipients') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add Recipients</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection