<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SemaSms | Log in</title>
  <!-- preventing the back button -->
  <meta http-equiv="cache-control" content="private, max-age=0, no-cache, no-cache">
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="pragma" content="no-store">
  <meta http-equiv="expires" content="0">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/dist/css/admin-lte.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href=" {{ asset('/bower_components/admin-lte/plugins/iCheck/square/blue.css') }}">
  <!-- skin -->
  <link href="{{ asset('/bower_components/admin-lte/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<div class="header" style="height: 20px;">
  <div class="col-xs-6">
   
  </div>
</div>
<body class="skin-black-light" style="font-family: 'ContextLightSSi';">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/')}}"><img src="images/semasms.png" width="200" height="100"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign In to <b>SemaSms</b></p>
      @if ($alert = Session::get('alert-success'))
            <div class="alert alert-success">
                <span class="glyphicon glyphicon-ok"></span>
                <em>{!! $alert !!}</em>
            </div>
      @endif

    <form action="{{ url('login')}}" method="post">
      <div class="form-group has-feedback">
         {{ csrf_field() }}
      </div>
      <div class="form-group has-feedback">
        <input type="email"  name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($error = $errors->first('email'))
          <div class="alert alert-danger">{{ $errors->first('email') }}</div>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($error = $errors->first('password'))
          <div class="alert alert-danger">{{ $errors->first('password') }}</div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-6">
        	<a href="{{ URL::to('/') }}" class="btn btn-danger btn-block btn-flat">Cancel</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="{{ url('forgot') }}">I forgot my password</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src=" {{ asset('/bower_components/admin-lte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/bower_components/admin-lte/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('/bower_components/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>

</body>
</html>
