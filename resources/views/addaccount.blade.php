@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Add Account</h3>
        			<form action="{{url('addaccount')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-4">
                				<label>Organisation:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-university"></i>
				                  </div>
		                          	<select name="organisation" class="form-control" placeholder="Organisation">
									@foreach($orgs as $org)
								      <option value="{{ $org['id'] }}">{{ $org['name'] }}</option>
									@endforeach
									</select>

				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
						 	<div class="form-group col-sm-4">
                				<label>User Type:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-tag"></i>
				                  </div>
		                          	<select name="account_type" class="form-control" placeholder="Account Type">
									@foreach($accounttypes as $accounttype)
								      <option value="{{ $accounttype['id'] }}">{{ $accounttype['name'] }}</option>
									@endforeach
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Status:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-tag"></i>
				                  </div>
		                          	<select name="status" class="form-control" placeholder="Status">
								      <option value="0">Disabled</option>
								      <option value="1">Enabled</option>
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
        					<div class="form-group col-sm-4">
                				<label>Sender Id:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-envelope"></i>
				                  </div>
				                  <input type="text" class="form-control" name="sender_id" placeholder="Sender">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Username:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
				                  <input type="text" class="form-control" name="username" placeholder="Username">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-4">
                				<label>Password:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-key"></i>
				                  </div>
				                  <input type="password" class="form-control" name="password" placeholder="*******">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('accounts') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add Account</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection