@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Add Organisation</h3>
        			<form action="{{url('addorg')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>Organisation Type:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-user"></i>
				                  </div>
  				                  <select name="org_type" class="form-control" placeholder="Org Type">
									@foreach($orgtypes as $orgtype)
								      <option value="{{ $orgtype['id'] }}">{{ $orgtype['name'] }}</option>
									@endforeach
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				</div><!-- /.input group -->
          				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>Name:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-university"></i>
				                  </div>
				                  <input type="text" class="form-control" name="name" placeholder="Organisation Name">
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
          				</div><!-- /.input group -->
          				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>County:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-map-marker"></i>
				                  </div>
				                  <select name="county" class="form-control" placeholder="County">
									@foreach($counties as $county)
								      <option value="{{ $county['id'] }}">{{ $county['county_name'] }}</option>
									@endforeach
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('orgs') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection