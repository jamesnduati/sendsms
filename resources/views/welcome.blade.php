<!DOCTYPE html>
<html lang="en">
<head>
<title>SemaSms | Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bulksms, Sms, Voice, Email marketing, Web application, Mobile Application, 
web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- search engine optimization -->
<meta name="description" content="SemaSms is a sms messaging solution by James Risk Management Ltd(JRM). It offers mobile messaging services, specializing in bulkSMS services to schools, churches, NGOs, SACCOs, SMEs, corporates and other organisations that may wish to use bulk sms as a mean of mass market communication.">
<!-- favicon -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<!-- bootstrap-css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<!--// css -->
<!-- font-awesome icons -->
<link href="font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Righteous&subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){   
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
</script>
</head>
<body style="font-family: 'ContextLightSSi';">
  <!-- header -->
  <div class="header">
    <div class="container">
      <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <div class="w3layouts-logo">
            <a href="{{url('/')}}"><img src="images/semasms.png" width="200" height="100"></a>
          </div>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
          <nav>
            <ul class="nav navbar-nav">
              <!-- <li class="active"><a href="index.html">Home</a></li> -->
              <li><a href="#" id="about" class="hvr-sweep-to-bottom">About</a></li>
              <li><a href="#" id="service" class="hvr-sweep-to-bottom"> Services</a></li>
             <!--  <li><a href="#" id="mail" class="hvr-sweep-to-bottom">Mail Us</a></li> -->
              <li><a class="fa fa-user" href="{{url('login')}}"> Login</a></li>
            </ul>
          </nav>
        </div>
        <!-- /.navbar-collapse -->
      </nav>
    </div>
  </div>
  <!-- //header -->
  <!-- banner -->
  <div class="banner">
    <div class="container">
      <section class="slider">
          <div class="flexslider">
            <ul class="slides">
              <li>  
                <div class="banner_text">
                  <h3>Welcome to SemaSms</h3>
                  <div class="w3ls-line"> </div>
                    <p>A bulksms solution that is tailored to your business needs.</p>
                  <div class="w3-button">
                    <!-- <a href="single.html" class="btn btn-1 btn-1b">Read More</a> -->
                  </div>
                </div>
              </li>
              <li>  
                <div class="banner_text">
                  <h3>BulkSms features:</h3>
                  <div class="w3ls-line"> </div>
                    <p>
                      <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>Branded Familiarity<br>
                      <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>Personalised Messaging<br>
                      <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>Cost Effective<br>
                      <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>Manageable Login<br>
                      <i class="glyphicon glyphicon-ok" aria-hidden="true"></i>Lifetime Validity<br>
                    </p>
                  <div class="w3-button">
                    <!-- <a href="single.html" class="btn btn-1 btn-1b">Read More</a> -->
                  </div>
                </div>
              </li>
            </ul>
          </div>
      </section>
      <!-- flexSlider -->
      <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
      <script defer src="js/jquery.flexslider.js"></script>
      <script type="text/javascript">
        $(window).load(function(){
          $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
            $('body').removeClass('loading');
            }
          });
        });
      </script>
      <!-- //flexSlider -->
    </div>
  </div>
  <!-- //banner -->
  <!-- about -->

  <!-- //about -->
  <!-- blog -->
  <div class="blog" id="service">
    <div class="container">
      <div class="services-heading">
        <h3>Our Services</h3>
        <div class="agileits-line"> </div>
      </div>
      <div class="agileinfo-blog-grids">
        <div class="col-md-4 wthree-blog">
          <div class="w3-agileits-blog">
            <div class="w3-agileits-blog-img">
              <a href="#"><img src="images/churches.jpg" alt="" /></a>
            </div>
            <div class="w3-agileits-blog-text">
              <a href="#">BulkSms for Churches</a>
              <p>Our bulksms solution is suitable for churches to help them in sending bulk sms to the congrgation.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 wthree-blog">
          <div class="w3-agileits-blog">
            <div class="w3-agileits-blog-img">
              <a href="#"><img src="images/schools.jpg" alt="" /></a>
            </div>
            <div class="w3-agileits-blog-text">
              <a href="#">BulkSms for Schools</a>
              <p>Our bulksms solution is suitable for schools to help them communicate with parents easily and in a timely manner.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 wthree-blog">
          <div class="w3-agileits-blog">
            <div class="w3-agileits-blog-img">
              <a href="#"><img src="images/saccos.jpg" alt="" /></a>
            </div>
            <div class="w3-agileits-blog-text">
              <a href="#">BulkSms for Saccos</a>
              <p>Our bulksms solution is suitable for saccos as it will enhance communication with their members easily and in a timely manner.</p>
            </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!-- //blog -->
  <!-- footer -->
  <div class="footer">
    <div class="container">
      <div class="agileinfo_footer_grids">
        <div class="col-md-4 agileinfo_footer_grid">
          <div class="agile-logo">
            <a href="index.html"><img src="images/semasms.png" width="200" height="100"></a>

          </div>
          <p>SemaSms is a sms messaging solution by James Risk Management Ltd(JRM). It offers mobile messaging services, specializing in bulkSMS services to schools, churches, NGOs, SACCOs, SMEs, corporates and other organisations.</p>

        </div>
        <div class="col-md-4 agileinfo_footer_grid">
          <h3>Contact Info</h3>
          <ul class="agileinfo_footer_grid_list">
            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>14 Riverside, Belgravia Centre, 3rd block, <span>Westlands, Nairobi</span></li>
            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@jrm.co.ke">info@jrm.co.ke</a></li>
            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+254788488142</li>
          </ul>
        </div>
        <div class="col-md-4 agileinfo_footer_grid">
          <h3>Social Links</h3>
          <div class="agileinfo-social-grids">
            <ul>
              <li><a href="https://www.facebook.com/jamesriskmanagement/"><i class="fa fa-facebook"></i></a></li>
              <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
              <!-- <li><a href="#"><i class="fa fa-rss"></i></a></li> -->
              <!-- <li><a href="#"><i class="fa fa-vk"></i></a></li> -->
            </ul>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="w3agile_footer_copy">
        <p>&copy; <?Php echo date('Y'); ?> SemaSms A product by James Risk Management Ltd. All rights reserved | Design by <a href="http://jrm.co.ke/">JRM</a></p>
      </div>
    </div>
  </div>
  <!-- //footer -->
  <script type="text/javascript" src="js/move-top.js"></script>
  <script type="text/javascript" src="js/easing.js"></script>
  <!-- here stars scrolling icon -->
  <script type="text/javascript">
    $(document).ready(function() {
      
        var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
        };
      
                
      $().UItoTop({ easingType: 'easeOutQuart' });
                
      });
  </script>
<!-- //here ends scrolling icon -->
</body> 
</html>