 @extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">View All Users</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="usertable" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Surname</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th> 
							<th>Phone no.</th>
							<th>Gender</th>
							<th>Id no.</th>
							<th>User Type</th>
							<th>Enabled</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($users as $user)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                <td>{{ $user['surname']}}</td>
			                <td>{{ $user['first_name']}}</td>
			                <td>{{ $user['last_name']}}</td>
			                <td>{{ $user['email']}}</td>
			                <td>{{ $user['phone_number']}}</td>
			                <td>{{ $user['gender']}}</td>
			                <td>{{ $user['id_number']}}</td>
			                @foreach($usertypes as $usertype)
			                	@if($user['user_type'] == $usertype['id'])
			                		<td>{{ $usertype['name'] }}</td>
		                		@endif
			                @endforeach	
			                @if($user['enabled'] == 1)
			                	<td>
			                		<button type="button" class="btn btn-success" onClick="disableUser({{ $user['id'] }})">
			                		Disable
									</button>
			                	</td>
			                @else
			                	<td>
			                		<button type="button" class="btn btn-danger" onClick="enableUser({{ $user['id'] }})">
			                		Enable
									</button>
								</td>
			                @endif                
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- disable or enable users modal -->
        <!-- Button trigger modal -->

      </div>
      <!-- /.row -->
@endsection