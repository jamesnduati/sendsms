@extends('dashboard')


@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">My Recipients</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="recipient" class="display responsive nowrap" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Name</th>
							<th>Phone</th>
							<th>Added By</th>
							<th></th>
							<th></th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($recipients as $recipient)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
			                <td>{{ $recipient['name'] }}</td>
			                <td>{{ $recipient['phone'] }}</td>
			                @foreach($users as $user)
			                	@if($recipient['added_by'] == $user['id'])
			                		<td>{{ $user['first_name'].' '.$user['last_name'] }}</td>
		                		@endif
			                @endforeach
			                <td>

			                	<button type="button" id="{{ $recipient['id']}}" class="btn btn-warning" data-toggle="modal" data-target="#editRecipient">Edit</button>
			                </td>	 
			                <td>
			                	<button type="button" class="btn btn-danger" onClick="deleteRecipient({{ $recipient['id'] }})">Delete
			                	</button>
			                </td>          
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection