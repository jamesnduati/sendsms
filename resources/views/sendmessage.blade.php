@extends('dashboard')

@section('content')
  <!-- row -->
  <div class="row">
  	<!-- col -->
        <div class="col-md-12">
        	<div class="box box-danger">
        		<div class="box-header">
        			<h3 class="box-title">Send Message</h3>
        			<form action="{{url('sendmessage')}}" method="POST">
        			{{ csrf_field() }}
        				<div class="row">
			              	<div class="form-group col-sm-6">
                				<label>Recipients:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-users"></i>
				                  </div>
									<select id="recipients" name="recipient" multiple="multiple">
									@foreach($recipients as $recipient)
								      <option value="{{ $recipient['id'] }}">{{ $recipient['name'] }}</option>
									@endforeach
									</select>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
						 	<div class="form-group col-sm-6">
                				<label>Text:</label>
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-comments"></i>
				                  </div>
				                  	<textarea class="form-control" id="message" name="message" rows="3">
				                  		
				                  	</textarea>

				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        				<div class="row">
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <a href="{{ url('messages') }}" class="btn btn-success btn-block btn-flat">Cancel</a>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
			              	<div class="form-group col-sm-6">
				                <div class="input-group col-sm-6">
				                  <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
				                </div><!-- /.input group -->
              				</div><!-- /.form group -->
        				</div><!-- /.input group -->
        			</form>
    			</div>
			</div>
		</div>
	</div>
  <!-- /.row -->
@endsection