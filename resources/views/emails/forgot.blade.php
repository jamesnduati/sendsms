<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SemaSMS | Forgot Password Email</title>
 	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/admin-lte.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <!-- Datatables -->
    <link href="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}" rel="stylesheet" type="text/css" >
     <link href="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.css") }}" rel="stylesheet" type="text/css" >
    <!-- skin -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="wrapper">
		<p><b>Welcome .{{ $user->firt_name}}.</b><br> Your updated password is.
		</p>
		<div class="row">
			<div class="col-sm-3">
				<p>Name : {{$user->surname.' '.$user->first_name.' '.$user->last_name}}</p>
			</div>
			<div class="col-sm-3">
				<p>Email : {{$user->email}}</p>
				<p>Phone Number : {{$user->phone_number}}</p>
			</div>
			<div class="col-sm-3">
				<p>Id Number : {{$user->id_number}}</p>
				<p>Password : {{$new_password}}</p>
			</div>
			<div class="col-sm-3">
				@foreach($orgs as $org)
					@if($org['id'] == $user->organisation )
						<p>Organisation : {{ $org['name'] }}</p>
					@endif
				@endforeach
			</div>
		</div>
		<p>Kindly proceed to login the system and change your password.<br>Thank you and do enjoy our services.
		</p>
	</div>


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- Datatables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#usertable').DataTable();
    } );
    
</script>
<!-- /Datatables -->
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>