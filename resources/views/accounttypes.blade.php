@extends('dashboard')

@section('content')
   <div class='row'>
        <div class='col-md-12'>
          	<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">All Accounts Types</h3>
        		</div>
            <!-- /.box-header -->
            <div class="box-body">
              	<table id="accounttype" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>No.</th>
							<th>Name</th>
							<th>No of Users</th>
							<th>Price</th>
			            </tr>
			        </thead>
			        <tbody>
			        <?Php $counter=1;?>
			        @foreach($accounttypes as $accounttype)
			            <tr>
			                <td><?Php echo $counter++; ?></td>
	                		<td>{{ $accounttype['name'] }}</td>
	                		<td>{{ $accounttype['no_of_users']}}</td>
	                		<td>{{ $accounttype['price'] }}</td>           
			            </tr>
			        @endforeach
			        </tbody>
			    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection