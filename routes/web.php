<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes
Auth::routes();

//public routes
//login route
Route::get('login',   'Auth\LoginController@login');
//post login route
Route::post('login', 'Auth\LoginController@postLogin');
//logout route
Route::get('logout', 'Auth\LoginController@logout');
// generate new password 
Route::get('forgot', 'ForgotPasswordController@create');
//post the forgot password request
Route::post('forgot', 'ForgotPasswordController@postCreate');
Route::group(['middleware' => 'auth'], function()
{
	Route::group(['middleware' => 'revalidate'], function()
	{
	    Route::get('dashboard', array('as' => 'Home', 'uses' => 'DashboardController@index'));
		// change the password
		Route::get('changepassword', array('as' => 'ChangePassword' , 'uses' => 'ForgotPasswordController@index'));
		// change the password post
		Route::post('changepassword', 'ForgotPasswordController@store');

		// getting all the users in the system
		Route::get('users', array('as' => 'Users', 'uses' => 'UsersController@index'));
		//add a users
		Route::get('adduser', array('as' => 'AddUser', 'uses' =>  'UsersController@create'));
		Route::post('adduser', 'UsersController@store');
		//enable a user
		Route::get('enableuser/{id}', 'UsersController@enableUser');
		//disable a user
		Route::get('disableuser/{id}', 'UsersController@disableUser');

		// getting all the orgs in the system
		Route::get('orgs', array('as' => 'Organisations', 'uses' =>  'OrganisationController@index'));
		//add an organisation
		Route::get('addorg', array('as' => 'AddOrganisation', 'uses' => 'OrganisationController@create'));
		Route::post('addorg', 'OrganisationController@store');

		// getting all the orgtypes in the system
		Route::get('orgtypes', array('as' => 'OrganisationTypes', 'uses' => 'OrganisationTypeController@index'));
		//add an organisationtype
		Route::get('addorgtype', array('as' => 'AddOrgType', 'uses' => 'OrganisationTypeController@create'));
		Route::post('addorgtype', 'OrganisationTypeController@store');

		// getting all the accounts in the system
		Route::get('accounts', array('as' => 'Accounts', 'uses' =>  'AccountsController@index'));
		//add an account
		Route::get('addaccount', array('as' => 'AddAccount', 'uses' =>  'AccountsController@create'));
		Route::post('addaccount', 'AccountsController@store');

		//enable an account
		Route::get('enableaccount/{id}', 'AccountsController@enableAccount');
		//disable an account
		Route::get('disableaccount/{id}', 'AccountsController@disableAccount');

		// getting all the account types in the system
		Route::get('accounttypes', array('as' => 'AccountTypes', 'uses' => 'AccountTypeController@index'));

		// sent sms messages
		Route::get('messages', array('as' => 'Messages', 'uses' =>  'MessageController@index'));
		// sending an sms message
		Route::get('sendmessage', array('as' => 'SendMessage', 'uses' =>  'MessageController@create'));

		// sending an sms message
		Route::post('sendmessage', 'MessageController@store');

		// view my recipients
		Route::get('recipients', array('as' => 'Recipients', 'uses' => 'RecipientController@index'));
		//edit my recipient
		Route::get('recipient/{id}', 'RecipientController@index');
		Route::get('addrecipient', array('as' => 'AddRecipient', 'uses' => 'RecipientController@create'));
		Route::get('addrecipientfile', array('as' => 'AddRecipientExcel', 'uses' =>  'RecipientController@createExcel'));
		// posting the excel file
		Route::post('addrecipientfile', array('as' => 'AddRecipientExcel', 'uses' =>  'RecipientController@postExcel'));
		//posting the recipients input form
		Route::post('addrecipient', 'RecipientController@store');
		//delete a recipient
		Route::get('deleterecipient/{id}', 'RecipientController@destroy');
		/*
		* dashboard api's
		*/
		//getting the total registered users 
		Route::get('registeredusers', 'DashboardController@getUsers');
		//getting the total messages sent by a user 
		Route::get('getmytotalmessages', 'DashboardController@getMyTotalMessages');
		//getting the account balance 
		Route::get('getaccountbalance', 'DashboardController@getAccountBalance');
	});


});


